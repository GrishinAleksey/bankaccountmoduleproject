package exceptions;

public class TerminalException extends Exception{
    public TerminalException(String msg ){
        super(msg);
    }
}
