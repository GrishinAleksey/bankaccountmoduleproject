package service;

import entity.Client;
import exceptions.AbsentMultiplyException;
import exceptions.NullBalanceException;
import exceptions.OutOfBalanceException;

public interface Terminal {
    double getMoney(Client client);
    void depositMoney(Client client, double money) throws AbsentMultiplyException, NullBalanceException;
    void withDrawMoney(Client client, double money) throws NullBalanceException, AbsentMultiplyException, OutOfBalanceException;
}
