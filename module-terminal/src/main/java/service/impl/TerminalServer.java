package service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import entity.Client;
import exceptions.AbsentMultiplyException;
import exceptions.NullBalanceException;
import exceptions.OutOfBalanceException;

public class TerminalServer {

    public void toDeposit(Client client, double amount){
        client.setBalance(client.getBalance() - amount);
    }

    public void withdraw(Client client, double amount) throws OutOfBalanceException {
        if(amount > client.getBalance())
            throw new OutOfBalanceException("Недостаточно средств");
        client.setBalance(client.getBalance() - amount);
    }


}
