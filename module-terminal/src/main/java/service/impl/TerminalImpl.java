package service.impl;

import entity.Client;
import exceptions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.Terminal;
import validator.PinValidator;

public class TerminalImpl implements Terminal {

    Terminal terminal;
    public static final Logger LOGGER = LoggerFactory.getLogger(TerminalImpl.class);
    static PinValidator pinValidator = new PinValidator();
    private final TerminalServer server = new TerminalServer();

    public TerminalImpl(){}
    public TerminalImpl(Terminal terminal) {
        this.terminal = terminal;
    }

    public void authorize(Client client){
        try {
            pinValidator.validate(client);
        } catch (BadPinException e) {
            System.out.println("Неверный пин");
            LOGGER.debug("BadPinException", e);
            authorize(client);
        } catch (PinLockException e) {
            System.out.println("Время до разблокировки: " + e.timeRemaining());
            LOGGER.debug("PinLockException", e);
            authorize(client);
        }
    }

    public void endSession() {
        pinValidator.reset();
    }

    @Override
    public double getMoney(Client client) {
        return client.getBalance();
    }

    @Override
    public void depositMoney(Client client, double money){
        try {
            checkAmount(money);
            checkBalance(client);
            server.toDeposit(client, money);
        } catch (AbsentMultiplyException e) {
            System.out.println("Сумма не кратна 100");
            LOGGER.debug("AbsentMultiplyException", e);
        } catch (NullBalanceException e) {
            System.out.println("Баланс пользователя равен 0");
            LOGGER.debug("NullBalanceException", e);
        }
    }

    @Override
    public void withDrawMoney(Client client, double money){
        try {
            checkAmount(money);
            checkBalance(client);
            server.withdraw(client, money);
        } catch (AbsentMultiplyException e) {
            System.out.println("Сумма не кратна 100");
            LOGGER.debug("AbsentMultiplyException", e);
        } catch (NullBalanceException e) {
            System.out.println("Баланс пользователя равен 0");
            LOGGER.debug("NullBalanceException", e);
        } catch (OutOfBalanceException e) {
            System.out.println("Введенная сумма превышает баланс");
            LOGGER.debug("OutOfBalanceException", e);
        }
    }

    public void checkAmount(double amount) throws AbsentMultiplyException {
        if (amount % 100 > 0)
            throw new AbsentMultiplyException("Сумма не кратна 100");
    }

    public void checkBalance(Client client) throws NullBalanceException {
        if (client.getBalance() == 0)
            throw new NullBalanceException("Баланс пользователя равен 0");
    }

}
