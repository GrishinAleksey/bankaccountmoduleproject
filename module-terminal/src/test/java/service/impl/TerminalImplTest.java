package service.impl;

import entity.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import service.Terminal;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TerminalImplTest {

    Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);


    @Test
    public void depositMoney() {
        double enterSum = 5000;
        double amount = client.getBalance() + enterSum;
        assertEquals(15000, amount, 000001d);
    }

    @Test
    public void withDrawMoney() {
        double enterSum = 5000;
        double amount = client.getBalance() - enterSum;
        assertEquals(5000, amount, 000001d);
    }

}