package service.impl;

import entity.Client;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import service.Terminal;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TerminalImplMockitoTest {
    @Mock
    Terminal terminal;

    TerminalImpl terminalImpl;
    Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);

    @Before
    public void setUpClass(){
        terminalImpl = new TerminalImpl(terminal);
    }

    @Test
    public void testCheckBalance() {
        Mockito.when(terminal.getMoney(client)).thenReturn(100d);
        double amount = terminal.getMoney(client);
        boolean result = false;
        if (amount != 0){
            result = true;
        }
        assertTrue(result);
    }
}
