package service.impl;

import entity.Client;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import service.Terminal;

import java.util.UUID;

import static org.junit.Assert.*;

public class TerminalServerTest {

    Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);

    @Test
    public void toDeposit() {
        double enterSum = 5000;
        double amount = client.getBalance() + enterSum;
        assertEquals(15000, amount, 000001d);
    }

    @Test
    public void withdraw() {
        double enterSum = 5000;
        double amount = client.getBalance() - enterSum;
        assertEquals(5000, amount, 000001d);
    }
}