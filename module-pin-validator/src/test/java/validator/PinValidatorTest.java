package validator;

import entity.Client;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class PinValidatorTest {

    @Test
    public void validate() {
        Client client = new Client(UUID.randomUUID(), "Сергей", 10000, 1234);
        int pinIn = 1234;
        int pin = client.getPinCode();
        boolean isValid = false;
        if(pinIn == pin){
            isValid = true;
        }
        Assert.assertTrue(isValid);
    }
}