package entity;

import java.util.UUID;

public class Client{

    private final UUID id;
    private final String firstName;
    private double balance;
    private int pinCode;

    public Client(UUID id, String firstName, double balance, int pinCode) {
        this.pinCode = pinCode;
        this.id = UUID.randomUUID();
        this.firstName = firstName;
        this.balance = balance;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public UUID getId() {
        return id;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getFirstName() {
        return firstName;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "{\"_class\":\"Account\", " +
                "\"id\":" + (id == null ? "null" : id) + ", " +
                "\"firstName\":" + (firstName == null ? "null" : "\"" + firstName + "\"") + ", " +
                "\"balance\":\"" + balance + "\"" +
                "}";
    }
}
