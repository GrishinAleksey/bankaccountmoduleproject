package validator;

import entity.Client;
import exceptions.BadPinException;
import exceptions.PinLockException;

import java.util.Scanner;


public class PinValidator {

    private int missCount = 0;
    private boolean isLocked = false;
    private boolean valid = false;
    private Long lock = null;

    public void reset() {
        missCount = 0;
        isLocked = false;
    }

    public void validate(Client client) throws BadPinException, PinLockException {

        System.out.println("Пин-код");
        Scanner sc = new Scanner(System.in);
        int pin = sc.nextInt();

        if (isLocked) {
            long current = System.currentTimeMillis();
            long timeToUnlock = lock - current;
            if (timeToUnlock > 0) {
                throw new PinLockException(timeToUnlock);
            } else {
                isLocked = false;
                missCount = 0;
            }
        }

        if (pin == client.getPinCode()) {
            missCount = 0;
            valid = true;
        } else {
            valid = false;
            if (++missCount == 2) {
                isLocked = true;
                lock = System.currentTimeMillis() + 5000L;
            }
            throw new BadPinException("Неверный пин код");
        }
    }

    public void check() throws BadPinException {
        if (!this.valid) {
            throw new BadPinException("Неверный пин код");
        }
    }

}
